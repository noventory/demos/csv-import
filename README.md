# Noventory

## CSV import demo

Welcome! This repository is a demonstration about the [Noventory.io](https://noventory.io/) API, with the use case of a CSV import.

## API Documentation

- [Starting using the API](https://help.noventory.io/doc/api-starting)
- [Manipulating objects](https://help.noventory.io/doc/api-objects)
