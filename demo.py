#!/usr/bin/env python3
# coding: utf-8

# Imports
import requests
import sys
import re
import json
import glob
import os
import csv

# Configuration
api = {
    'site': '<site>',
    'account': '<account>',
    'secret': '<secret>',
    'token': None
}


# Globals
_sc = 0
_response = None
_type = None


# Query for API
def query(endpoint, **kwargs):

    # Globals
    global _sc, _response, _type

    # Init vars
    data = kwargs.get('data', None)
    data = json.dumps(data) if data else None
    method = kwargs.get('method', 'get').lower()

    # Headers
    headers = {
        'Content-Type': 'application/json'
    }
    if 'token' in api and api['token'] is not None:
        headers['Authorization'] = 'Bearer {}'.format(api['token'])

    # Build URL
    endpoint = endpoint[1:] if endpoint.startswith('/') else endpoint
    url = '{}/{}'.format(api['site'], endpoint)

    # Send query
    result = getattr(requests, method)(url, headers=headers, json=data)
    _sc = result.status_code
    if result.headers['Content-Type'] == 'application/json':
        _response = result.json()
        _type = 'json'
    else:
        _response = result.text
        _type = 'raw'

    # Return status code
    return _sc


# Format error
def get_error(msg=None, **kwargs):

    # Globals
    global _response, _type

    # Init vars
    ecode = kwargs.get('exit', -1)
    ecolor = kwargs.get('color', None)
    ecode = ecode if re.match(r'^\-?\d+$', str(ecode)) and int(ecode) >= -1 and ecode <= 255 else -1
    evoid = kwargs.get('void', False)

    # Get error in result
    if _type == 'json':
        error = _response.get('error', 'Unknown error')
        mfields = _response.get('missing_fields', None)
        if mfields:
            error = '{} (missing:{})'.format(error, mfields)
        ufields = _response.get('unsupported_fields', None)
        if ufields:
            error = '{} (unsupported:{})'.format(error, ufields)
    else:
        error = _response
    if evoid and msg:
        error = msg
        msg = None

    # Prepare message
    if msg:
        out = '{}: {}'.format(msg, error)
    else:
        out = error

    # Color
    colors = {
        'red': '\033[0;31;40m',
        'green': '\033[0;32;40m',
        'yellow': '\033[0;33;40m',
        'cyan': '\033[0;36;40m',
        'purple': '\033[0;35;40m'
    }
    if ecolor and ecolor in colors:
        out = '{}{}\033[0m'.format(colors[ecolor], out)

    # Display message
    print(out, file=sys.stderr)
    if ecode >= 0:
        sys.exit(ecode)


# Get bearer token
login_data = {'account':api['account'],'secret':api['secret']}
if query('/login', data=login_data, method='post') != 200:
    get_error('Could not login', color='red', exit=1)
api['token'] = _response.get('bearer_token')

# Parse files
for file in glob.glob('{}/data/*.csv'.format(os.path.dirname(__file__))):
    ot = os.path.basename(os.path.splitext(file)[0])
    with open(file, 'r') as hdr:
        r = csv.reader(hdr)
        row_id = -1
        fields = []
        for line in r:
            row_id += 1
            if row_id == 0:
                fields = line
                continue
            obj = {
                '_type': ot
            }
            skip_creation = False
            for i, f in enumerate(fields):
                if f.startswith('_parent:'):
                    if f.count(':') != 2:
                        get_error('When using _parent field, you must specify also the object type and the search term', color='red', exit=255, void=False)
                    _, pot, poa = f.split(':')
                    search_query = {'first': True, 'filter': '_type:{} {}:{}'.format(pot, poa, line[i])}
                    if query('/objects/search', data=search_query, method='post') == 200:
                        obj['_parent'] = _response['result']['object_id']
                        continue
                    else:
                        get_error(color='purple')
                        skip_creation = True
                        break
                elif f.startswith('_'):
                    continue
                else:
                    obj[f] = line[i]
            if not skip_creation:
                search_query = {'first': True, 'filter': '_type:{} {}:{}'.format(ot, fields[0], line[0])}
                if query('/objects/search', data=search_query, method='post') == 200:
                    get_error('Skip {}:{} ({}), duplicate object found'.format(file, row_id, line[0]), void=True, color='cyan')
                    continue
                if query('/object', data=obj, method='post') == 200:
                    get_error('{}:{} ({}) created'.format(file, row_id, line[0]), void=True, color='green')
                else:
                    get_error('Could not create object {}:{}'.format(file, row_id), color='red', exit=2)
            else:
                get_error('Skip {}:{} ({}), maybe a missing parent object?'.format(file, row_id, line[0]), void=True, color='yellow')
        hdr.close()
